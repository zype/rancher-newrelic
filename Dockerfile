FROM debian:8
MAINTAINER Jeff Moody <jmoody@zype.com>

ENV NEW_RELIC_LICENSE_KEY YOUR_LICENSE_KEY
ENV LOG_LEVEL info
ENV NEW_RELIC_VERSION 2.3.0.132
ENV NEW_RELIC_CONFIG_PATH /etc/newrelic/nrsysmond.cfg

WORKDIR /root
RUN apt-get update && apt-get install -y curl wget
RUN mkdir /host/
RUN echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list
RUN wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -
RUN apt-get update && apt-get install newrelic-sysmond=$NEW_RELIC_VERSION && apt-get clean

RUN echo cgroup_root=\"/host/sys/fs/cgroup\" >> $NEW_RELIC_CONFIG_PATH

CMD /usr/sbin/nrsysmond-config --set license_key=$NEW_RELIC_LICENSE_KEY && \
  /usr/sbin/nrsysmond -c $NEW_RELIC_CONFIG_PATH -n $(curl http://169.254.169.254/latest/meta-data/instance-id) -d $LOG_LEVEL -l /dev/stdout -f